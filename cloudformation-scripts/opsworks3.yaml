---
AWSTemplateFormatVersion: '2010-09-09'
Description: Genesys Opsworks Template
Parameters:
  KeyName:
    Type: AWS::EC2::KeyPair::KeyName
  PrivateSubnet1:
    Type: String
  PrivateSubnet2:
    Type: String
  VpcId:
    Type: String
  ElkSecurityGroup:
    Type: String
    Description: Security Group for instances in ELK layer.
  MongoSecurityGroup:
    Type: String
    Description: Security Group for instances in Mongodb layer
  KafkazkSecurityGroup:
    Type: String
    Description: Security Group for instances in Kafka and Zookeeper layer
  MongoEBSSize:
    Type: String
    Default: '100'
    Description: Attach 100G Provisioned IOPS Volume to each Mongo Instance in a Cluster.
  MongoIops:
    Type: String
    Default: '300'
    Description: IOPS to use Mongo Proivisioned IOPS Volume. 
  ElkEBSSize:
    Type: String
    Default: '100'
    Description: Attach 100G EBS Volume to each ELK instance in the Cluster.
  KafkaEBSSize:
    Type: String
    Default: '100'
    Description: Attach 100G EBS Volume to each Kafka Instance in the Cluster.
  ZookeeperEBSSize:
    Type: String
    Default: '20'
    Description: Attach 100G EBS Volume to each Zookeeper Instance in the Cluster.
Resources:
  OpsWorksStack:
    Type: AWS::OpsWorks::Stack
    Properties:
      Name: !Sub "${AWS::StackName}-stack"
      ServiceRoleArn:
        Fn::GetAtt:
        - OpsWorksServiceRole
        - Arn
      DefaultInstanceProfileArn:
        Fn::GetAtt:
        - OpsWorksInstanceProfile
        - Arn
      DefaultSshKeyName:
        Ref: KeyName
      DefaultOs: CentOS Linux 7
      DefaultRootDeviceType: ebs
      VpcId:
        Ref: VpcId
      DefaultSubnetId:
        Ref: PrivateSubnet1
      UseCustomCookbooks: true
      UseOpsworksSecurityGroups: false
      CustomCookbooksSource:
        Type: git
        Url: git@github.com:solariat/aws-deploy.git
        Revision: master
      ConfigurationManager:
        Name: Chef
        Version: '12'
  ElkLayer:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: elk
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: elk
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: gp2
        Size:
          Ref: ElkEBSSize
      CustomRecipes:
        Setup:
        - jop-elk::default
        Configure:
        - jop-elk::es-configure
      CustomSecurityGroupIds:
      - Ref: ElkSecurityGroup
  MongodbLayer:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongos
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongos
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::mongos
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoShardLayer1:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo1-shard
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo1-shard
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::shard1
        Configure:
        - jop-mongo::shard-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoShardLayer2:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo2-shard
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo2-shard
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::shard2
        Configure:
        - jop-mongo::shard-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoShardLayer3:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo3-shard
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo3-shard
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::shard3
        Configure:
        - jop-mongo::shard-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoDelayLayer1:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo1-delay
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo1-delay
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::shard1
        Configure:
        - jop-mongo::delay-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoDelayLayer2:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo2-delay
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo2-delay
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::shard2
        Configure:
        - jop-mongo::delay-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoDelayLayer3:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo3-delay
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo3-delay
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::shard3
        Configure:
        - jop-mongo::delay-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  MongoConfigLayer:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: mongo-config
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: mongo-config
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: io1
        Iops: !Ref MongoIops
        Size:
          Ref: MongoEBSSize
      CustomRecipes:
        Setup:
        - jop-mongo::configsrv
        Configure:
        - jop-mongo::configsrv-configure
      CustomSecurityGroupIds:
      - Ref: MongoSecurityGroup
  KafkaLayer:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: kafka
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: kafka
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: gp2
        Size:
          Ref: KafkaEBSSize
      CustomRecipes:
        Setup:
        - jop-kafka::default
      CustomSecurityGroupIds:
      - Ref: KafkazkSecurityGroup
  ZookeeperLayer:
    Type: AWS::OpsWorks::Layer
    Metadata:
      Comment: OpsWorks instances require outbound Internet access. Using DependsOn
        to make sure outbound Internet Access is estlablished before creating instances
        in this layer.
    Properties:
      StackId:
        Ref: OpsWorksStack
      Name: zookeeper
      CustomInstanceProfileArn:
        Fn::GetAtt:
        - GenesysInstanceProfile
        - Arn
      Type: custom
      Shortname: zookeeper
      EnableAutoHealing: 'true'
      AutoAssignElasticIps: 'false'
      AutoAssignPublicIps: 'false'
      VolumeConfigurations:
      - MountPoint: "/data"
        NumberOfDisks: 1
        VolumeType: gp2
        Size:
          Ref: ZookeeperEBSSize
      CustomRecipes:
        Setup:
        -  jop-zookeeper::default
        Configure:
        - jop-zookeeper::configure
      CustomSecurityGroupIds:
      - Ref: KafkazkSecurityGroup
  OpsWorksServiceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - opsworks.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      Policies:
      - PolicyName: opsworks-service
        PolicyDocument:
          Statement:
          - Effect: Allow
            Action:
            - ec2:*
            - rds:Describe*
            - iam:PassRole
            - cloudwatch:GetMetricStatistics
            - elasticloadbalancing:*
            Resource: "*"
  OpsWorksInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
  GenesysInstanceRole:
     Type: AWS::IAM::Role
     Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
  GenesysInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - Ref: GenesysInstanceRole
  OpsWorksInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
      - Ref: OpsWorksInstanceRole

  zookeeper1:
    Type: AWS::OpsWorks::Instance
    Properties:
      StackId:
        Ref: OpsWorksStack
      LayerIds:
        - Ref: ZookeeperLayer
      InstanceType: "t2.small"
  zookeeper2:
    Type: AWS::OpsWorks::Instance
    Properties:
      StackId:
        Ref: OpsWorksStack
      LayerIds:
        - Ref: ZookeeperLayer
      InstanceType: "t2.small"
  zookeeper3:
    Type: AWS::OpsWorks::Instance
    Properties:
      StackId:
        Ref: OpsWorksStack
      LayerIds:
        - Ref: ZookeeperLayer
      InstanceType: "t2.small"
